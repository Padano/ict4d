import json
from pymongo import MongoClient
import requests

api_key = ''
req_url = 'http://api.openweathermap.org/data/2.5/weather?q={0},{1}&APPID={2}'


def import_weather_data():
    client = MongoClient('localhost', 27017)  # connect to MongoDB running on port 27017
    db = client['ict4d']  # get database 'ict4d'
    weather_collection = db['weather']  # get collection 'weather
    weather_data = weather_collection.find({})  # get all documents

    for region_weather in weather_data:  # iterate through weather data
        region = region_weather['name']
        country = region_weather['sys']['country']
        url = req_url.format(region, country, api_key)
        response = requests.get(url)  # query open weather data API

        if response.ok:  # if query succeeded
            content = json.loads(response.content)
            weather_collection.update({"_id": region_weather["_id"]}, {"$set": content})  # update weather info

if __name__ == '__main__':
    import_weather_data()
