def rainfallToRange(rainfall):
	if rainfall == 0:
		return 'No rainfall'
	elif rainfall >= 1 and rainfall <= 30:
		return 'Light rainfall'
	elif rainfall >= 31 and rainfall <= 50:
		return 'Average rainfall'
	elif rainfall >= 51 and rainfall <= 100:
		return 'Heavy rainfall'
	elif rainfall > 100:
		return 'Very heavy rainfall'

def windspeedToRange(windspeed):
	if windspeed >= 0 and windspeed <= 20:
		return 'Light wind'
	elif windspeed >= 21 and windspeed <= 50:
		return 'Average wind'
	elif windspeed >= 50 and windspeed <= 88:
		return 'Heavy wind'
	elif windspeed > 88:
		return 'Storm'
