# Prerequisites

* Python 2.7
* MongoDB
* Flask package for Python 2.7
* PyMongo package for Python 2.7
* Open Weather Data API key (sign up on their website for a free account)