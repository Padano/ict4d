import json
import requests
from pymongo import MongoClient

api_key = ''
req_url = 'http://api.openweathermap.org/data/2.5/weather?q={0}&APPID={1}'


def init():
    client = MongoClient('localhost', 27017)  # connect to MongoDB running on port 27017
    db = client['ict4d']  # get database 'ict4d'
    weather_collection = db['weather']  # get collection 'weather'
    regions_collection = db['regions']
    strings_collection = db['strings']
    weather_collection.delete_many({})  # clear all documents
    regions_collection.delete_many({})
    strings_collection.delete_many({})

    with open('regions.json') as regions_file:
        data = json.load(regions_file)
        regions_collection.insert_one(data)

    with open('weather_strings.json') as strings_file:
        data = json.load(strings_file)
        strings_collection.insert_one(data)
        districts = regions_collection.find_one({})

        for district in districts['regions']:
            for region in districts['regions'][district]:
                url = req_url.format(region + ",BF", api_key)
                response = requests.get(url)  # query open weather data API

                if response.ok:  # if query succeeded
                    content = json.loads(response.content)
                    weather_collection.insert_one(content)  # update weather info

if __name__ == '__main__':
    init()
